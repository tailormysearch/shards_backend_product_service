Shards Product Search Service
=============================

Part of the microservice architecture of the shards backend. Provides search functionality for all our products existing in the elasticsearch database. The project runs on port _9010_.

This service depends upon 2 other projects:

 - [Shards datamodel](https://bitbucket.org/Hiddevb/shards_backend_datamodel)
 - [Shards Eureka server](https://bitbucket.org/Hiddevb/shards_backend_api_gateway)

## Technologies ##

The project is build with the [Spring Tool Suite](https://spring.io/tools).

 - [Spring boot](https://projects.spring.io/spring-boot/)
 - [Gradle](https://gradle.org/)
 - [Elasticsearch](https://www.elastic.co/)
 - [Eureka](https://github.com/netflix/eureka) from the netflix stack

## Getting Started ##

### Installation ###

To succesfully run the project you will first need to fetch the dependancies listed above. After that you'll need:

 - [Java](https://www.java.com) (at least 1.7)
 - [Gradle](https://gradle.org/)

All other dependancies should be handled by Gradle.

### Build and Run ###

run the command `gradle bootRun` in your console.

---