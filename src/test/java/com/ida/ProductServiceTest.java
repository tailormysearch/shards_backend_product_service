package com.ida;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import com.ida.dto.ProductDTO;

import be.ida.model.Product;
import be.ida.repository.ProductRepository;
import be.ida.service.ProductServiceImpl;

/*
 * Service unit test
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceTest {

	@SuppressWarnings("unused")
	private Pageable pageable;
	private Product p1, p2, p3;
	private List<Product> products;

	@Mock
	private ProductRepository repository;

	@InjectMocks
	private ProductServiceImpl service = new ProductServiceImpl();

	@Before
	public void setup() {
		initProducts();
		System.out.println(service);
	}

	private void initProducts() {
		p1 = new Product(1, "first", "foo");
		p2 = new Product(2, "second", "foo");
		p3 = new Product(3, "third", "bar");
		products = new ArrayList<Product>();
		products.add(p1);
		products.add(p2);
		products.add(p3);

		pageable = new PageRequest(0, 24);
		repository.save(p1);
		repository.save(p2);
		repository.save(p3);
		System.out.println(repository.findAll());
	}

	// findOne Tests \\

	@Test
	public void returnSingleProductSuccess() {
		int id = p1.getId();
		ProductDTO product = service.getOne(id);

		assertNotNull(product);
		assertEquals(product.getId(), id);
		assertEquals(product, p1);
	}

	@Test
	public void returnSingleProductFail() {
		int id = -1;
		ProductDTO product = service.getOne(id);

		assertNull(product);
	}

	// findAll Tests \\

	// @Test
	// public void findAllOnePage() {
	// pageable = new PageRequest(0, products.size()+1);
	// Page<Product> newProducts = service.getAll(pageable);
	//
	// assertEquals(products.size(), newProducts.getContent().size());
	// assertEquals(products, newProducts);
	// }
	//
	// @Test
	// public void findAllMultiPage() {
	// int firstPageSize = (int)(products.size()/2) + 1;
	// int secondPageSize = products.size() - firstPageSize;
	//
	// pageable = new PageRequest(0, firstPageSize);
	// Page<Product> firstPage = service.getAll(pageable);
	//
	// pageable = new PageRequest(1, firstPageSize);
	// Page<Product> secondPage = service.getAll(pageable);
	//
	// assertEquals(firstPage.getContent().size(), firstPageSize);
	// assertEquals(secondPage.getContent().size(), secondPageSize);
	// }
	//
	// // findbyCategoryOrName Tests
	//
	// @Test
	// public void findbyCategoryOrNameNoParameters() {
	// Page<Product> newProducts = service.filter(1, null, null, null, null,
	// pageable);
	//
	// assertEquals(newProducts, products);
	// assertEquals(newProducts, service.getAll(pageable));
	// }
}
