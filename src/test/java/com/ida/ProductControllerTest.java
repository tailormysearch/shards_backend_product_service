package com.ida;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import be.ida.controller.ProductController;
import be.ida.model.Product;
import be.ida.service.ProductService;

/*
 * Controller Unit test
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductControllerTest {

	@Autowired
	private ProductController controller;

	@Autowired
	private MockMvc mvc;

	@MockBean
	private ProductService service;

	// private Pageable pageable;
	private Product p1, p2, p3;
	private List<Product> products;

	@Before
	private void setup() {
		initProducts();
	}

	private void initProducts() {
		p1 = new Product(1, "first", "foo");
		p2 = new Product(2, "second", "foo");
		p3 = new Product(3, "third", "bar");
		products = new ArrayList<Product>();
		products.add(p1);
		products.add(p2);
		products.add(p3);

		// pageable = new PageRequest(0, 24);
	}

	@Test
	public void contextLoads() {
		assertThat(controller).isNotNull();
	}

	@Test
	public void ControllerFindsAllResults() throws Exception {
		// when(service.getAll(pageable)).thenReturn(products);
		mvc.perform(get("/")).andDo(print()).andExpect(status().isOk());
	}

	@Test
	public void ControllerFindsSingleResult() throws Exception {
//		when(service.getOne(1)).thenReturn(p1);
//		when(service.getOne(2)).thenReturn(p2);
//		when(service.getOne(3)).thenReturn(p3);

		mvc.perform(get("/1")).andDo(print()).andExpect(status().isOk());
	}

}
