package be.ida.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import com.ida.dto.ProductDTO;
import com.ida.dto.ProductPageDTO;

import be.ida.service.ProductService;

@RestController
public class ProductController {
	private final Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private ProductService productService;

	@Autowired
	private RequestMappingHandlerMapping requestMappingHandlerMapping;

	@GetMapping("/")
	public String defaultGreeting() {
		String greeting = "<h1Product productService</h1><p>Mappings:</p><ul>";
		for (RequestMappingInfo m : requestMappingHandlerMapping.getHandlerMethods().keySet()) {
			greeting += "<li>" + m + "</li>";
		}
		greeting += "</ul>";
		return greeting;
	}
	
	/*
	 * Mappings for CRUD functions
	 */

	@GetMapping("/product/{id}")
	public ProductDTO getOne(@PathVariable long id) {
		logger.info("Get single product with id:" + id);
		return productService.getOne(id);
	}

	@GetMapping(value = "/filter", produces = MediaType.APPLICATION_JSON_VALUE)
	public ProductPageDTO filter(@RequestParam(value = "p", required = false, defaultValue = "0") int page,
			@RequestParam(value = "l", required = false, defaultValue = "24") int limit,
			@RequestParam(value = "pref", required = false, defaultValue = "None") String preferences,
			@RequestParam(value = "sortF", required = false, defaultValue = "recommended") String sortField,
			@RequestParam(value = "sortO", required = false, defaultValue = "ASC") String sortOrder,
			@RequestParam(value = "b", required = false) String brand,
			@RequestParam(value = "s", required = false) String subCategory,
			@RequestParam(value = "c", required = false) String category) {
		Pageable pageable = new PageRequest(page, limit);
		logger.info("Get all "+category+" in subCategory " + subCategory + " and brand " + brand);
		return productService.filter(preferences, category, subCategory, brand, sortField, sortOrder, pageable);
	}

	@GetMapping(value = "/search", produces = MediaType.APPLICATION_JSON_VALUE)
	public ProductPageDTO search(@RequestParam(value = "q", required = true) String query,
			@RequestParam(value = "p", required = false, defaultValue = "0") int page,
			@RequestParam(value = "l", required = false, defaultValue = "24") int limit,
			@RequestParam(value = "c", required = false) String category) {
		Pageable pageable = new PageRequest(page, limit);
		logger.info("query on the terms:" + query);
		return productService.search(query, category, pageable);
	}

	@GetMapping("/brands")
	public List<String> getBrands(@RequestParam(value = "c", required = false) String subCategory) {
		return productService.getBrands(null, subCategory);
	}

	@GetMapping("/categories")
	public List<String> getCategories() {
		return productService.getSubCategories(null);
	}
}