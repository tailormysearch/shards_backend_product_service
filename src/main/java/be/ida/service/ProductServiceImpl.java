package be.ida.service;

import static java.util.stream.Collectors.toList;
import static org.elasticsearch.index.query.MatchQueryBuilder.Operator.AND;
import static org.elasticsearch.index.query.QueryBuilders.*;
import static org.springframework.data.elasticsearch.core.query.Criteria.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;

import com.ida.dto.ProductDTO;
import com.ida.dto.ProductPageDTO;

import be.ida.model.Product;
import be.ida.repository.ProductRepository;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository productRepository;
	
	// Constants
	
	private static String noUserString = "recommended";
	private static String ascendingString = "ASC";
	private static String descendingString = "DESC";
	
	// Queries
	
	private String recommendedQuery = "{\n" +
									  "    \"sort\" : {\n" +
									  "        \"_script\" : {\n" +
									  "            \"type\" : \"number\", \"script\": {\n"+
									  "                \"lang\": \"painless\" \n"+
									  "                \"inline\": \"(params.ten*params.ten - doc[\"rating\"].value) + (doc[\"price\"].value/params.ten) - doc[\"isNew\"].value*params.ten*params.ten\" \n"+
									  "                \"params\": {\"ten\" : 10 } \n"+         
									  "            },\"order\" : \"asc\"\n" +
									  "	       }\n"+
									  "    }\n"+
									  "}\n";
	
	private String bigClientQuery =   "{\n" +
									  "    \"sort\" : {\n" +
									  "        \"_script\" : {\n" +
									  "            \"type\" : \"number\", \"script\": {\n"+
									  "                \"lang\": \"painless\" \n"+
									  "                \"inline\": \"(params.ten - doc[\"rating\"].value) + (doc[\"price\"].value/params.ten) - doc[\"isNew\"].value*params.ten*params.ten\" \n"+
									  "                \"params\": {\"ten\" : 10 } \n"+         
									  "            },\"order\" : \"asc\"\n" +
									  "	       }\n"+
									  "    }\n"+
									  "}\n";
	
	private String budgetClientQuery= "{\n" +
									  "    \"sort\" : {\n" +
									  "        \"_script\" : {\n" +
									  "            \"type\" : \"number\", \"script\": {\n"+
									  "                \"lang\": \"painless\" \n"+
									  "                \"inline\": \"(params.ten - doc[\"rating\"].value) + (doc[\"price\"].value/(params.ten*params.ten)) - doc[\"isNew\"].value*params.ten*params.ten\" \n"+
									  "                \"params\": {\"ten\" : 10 } \n"+         
									  "            },\"order\" : \"asc\"\n" +
									  "	       }\n"+
									  "    }\n"+
									  "}\n";
	
	/*
	 * Basic CRUD functions
	 */
	
	@Override
	public ProductDTO getOne(long id) {
		return mapToProductDTO(productRepository.findOne(id));
	}

	@Override
	public ProductPageDTO getAll(Pageable pageable) {
		return filter("None", null, null, null, null, null, pageable);
	}

	@Override
	public ProductPageDTO filter(String preferences, String category, String subCategory,
			String brand, String sortField, String sortOrder, Pageable pageable) {
		NativeSearchQueryBuilder searchQuery = new NativeSearchQueryBuilder();
		BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
		
		// Filtering
		if (StringUtils.isNotBlank(category)) { 
			boolQueryBuilder.must(matchQuery("category", category).operator(AND));
		}
		if (StringUtils.isNotBlank(subCategory)) {
			boolQueryBuilder.must(matchQuery("subCategory", subCategory).operator(AND));
		}
		if (StringUtils.isNotBlank(brand)) {
			boolQueryBuilder.must(matchQuery("brand", brand).operator(AND));
		}
		searchQuery.withQuery(boolQueryBuilder);
		
		// Sorting 
		if (sortField.equals(noUserString)){
			String sortQuery = recommendedQuery;
			if (preferences.equals("BUDGET")) {
				sortQuery = budgetClientQuery;
			} else if (preferences.equals("BIG")) {
				sortQuery = bigClientQuery;
			}
			System.out.println(sortQuery);
			searchQuery.withQuery(QueryBuilders.queryStringQuery(sortQuery));
		} else if (sortOrder.equals(ascendingString)) {
			searchQuery.withSort(SortBuilders.fieldSort(sortField).order(SortOrder.ASC));
		} else if (sortOrder.equals(descendingString)) {
			searchQuery.withSort(SortBuilders.fieldSort(sortField).order(SortOrder.DESC));
		}
		searchQuery.withPageable(pageable);
		Page<Product> productPage = productRepository.search(searchQuery.build());

		return mapToExternalProductPage(productPage, category, subCategory);
	}

	@Override
	public ProductPageDTO search(String query, String category ,Pageable pageable)  {
		NativeSearchQueryBuilder searchQuery = new NativeSearchQueryBuilder();
		BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
		
		// Search with focus on name, and less focus on the description'
		boolQueryBuilder.must(matchQuery("category", category).operator(AND));
		boolQueryBuilder.should(queryStringQuery(query).analyzeWildcard(true)
                        .field("name", 2.0f).field("subcategory").field("brand").field("description", 0.5f));
						
		searchQuery.withQuery(boolQueryBuilder);
		
		Page<Product> productPage = productRepository.search(searchQuery.build());
		
		return mapToExternalProductPage(productPage, null, null);
	}

	/*
	 * extra category functions
	 */

	@Override
	public List<String> getSubCategories(String category) {
		List<Product> products = new ArrayList<Product>(productRepository.findByCategory(category));
		Set<String> subCategories = new HashSet<String>();
		for (Product product : products) {
			subCategories.add(product.getSubCategory());
		}
		return new ArrayList<String>(subCategories);
	}

	@Override
	public List<String> getBrands(String category, String subCategory) {
		List<Product> products = new ArrayList<Product>(
				productRepository.
				findByCategoryAndSubCategory(category, subCategory));
		Set<String> brands = new HashSet<String>();
		for (Product product : products) {
			brands.add(product.getBrand());
		}
		return new ArrayList<String>(brands);
	}

	/*
	 * Mapping functions
	 */

	private ProductDTO mapToProductDTO(Product entity) {
		ProductDTO product = new ProductDTO();

		BeanUtils.copyProperties(entity, product);

		return product;
	}

	private ProductPageDTO mapToExternalProductPage(Page<Product> entity, String category, String subCategory) {
		ProductPageDTO page = new ProductPageDTO();

		int base = 24 * entity.getNumber();
		String productIntel = base + "-" + (base + entity.getNumberOfElements());
		page.setDescription("found " + entity.getTotalElements() + " products, showing " + productIntel);
		page.setPageNumber(entity.getNumber());
		page.setproductsCount(entity.getNumberOfElements());
		page.setTotalProductsCount((int) entity.getTotalElements());
		page.setContent(entity.getContent().stream().map(p -> mapToProductDTO(p)).collect(toList()));

		// Add brands and categories to reduce the amount of server calls
		page.setBrands(getBrands(category, subCategory));
		page.setSubCategories(getSubCategories(category));

		return page;
	}

}
