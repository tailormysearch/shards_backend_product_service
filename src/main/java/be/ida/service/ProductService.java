package be.ida.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.ida.dto.ProductDTO;
import com.ida.dto.ProductPageDTO;

public interface ProductService {
	public ProductDTO getOne(long id);

	public ProductPageDTO getAll(Pageable pageable);

	public ProductPageDTO search(String query, String cateogry ,Pageable pageable);

	public ProductPageDTO filter(String preferences, String category, String subCategory,
			String brand, String sortField, String sortOrder, Pageable pageable);

	// category > subCategory > brand //

	public List<String> getSubCategories(String category);

	public List<String> getBrands(String category, String subCategory);
}
