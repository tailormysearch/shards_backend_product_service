package be.ida.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;
import org.springframework.stereotype.Repository;

import be.ida.model.Product;

@Repository
public interface ProductRepository extends ElasticsearchCrudRepository<Product, Long> {
	Page<Product> findBySubCategoryAndBrand(String subCategory, String brand, Pageable pageable);
	Page<Product> findBySubCategory(String subCategory, Pageable pageable);
	Page<Product> findByBrand(String brand, Pageable pageable);

	List<Product> findByCategory(String category);
	List<Product> findByCategoryAndSubCategory(String category, String subCategory);
	List<Product> findByBrand(String brand);
	
	Page<Product> search(SearchQuery query);
}
