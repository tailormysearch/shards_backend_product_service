package be.ida.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "resource", type = "product")
public class Product {
	@Id
	private int id;
	
	private String name;
	private String productType;
	private String category;
	private String subCategory;
	private String brand;
	private double price;
	private String description;
	private String imageUrl;
	
	private int rating;
	private int isNew;

	public Product() {
	}

	public Product(int id, String name, String productType, String category, String subCategory, String brand,
			double price, String description, String imageUrl) {
		this();
		this.id = id;
		this.name = name;
		this.productType = productType;
		this.category = category;
		this.subCategory = subCategory;
		this.brand = brand;
		this.price = price;
		this.description = description;
		this.imageUrl = imageUrl;
	}

	public Product(int id, String name, String subCategory) {
		this(id, name, null, null, subCategory, null, 0.00, null, null);
	}
	
	// Getters

	public int getId() {
		return id;
	}

	public String getname() {
		return name;
	}

	public String getCategory() {
		return category;
	}

	public String getBrand() {
		return brand;
	}

	public double getPrice() {
		return price;
	}

	public String getDescription() {
		return description;
	}

	public String getProductType() {
		return productType;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public String getImageUrl() {
		return imageUrl;
	}
	
	public int getRating() {
		return rating;
	}
	
	public int getIsNew() {
		return isNew;
	}
	
	// Setters //

	public void setId(int id) {
		this.id = id;
	}

	public void setname(String name) {
		this.name = name;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	public void setRating(int rating) {
		this.rating = rating;
	}
	
	public void setIsNew(int isNew) {
		this.isNew = isNew;
	}

	@Override
	public String toString() {
		return id + " [" + name + " | " + category + "]";
	}	
}
